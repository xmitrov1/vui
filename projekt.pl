/* Pod akou národnou zástavou odchádza loď do Port Saidu? Ktorá loď vezie čaj?

1. Grécka loď odchádza o šiestej a vezie kávu.
2. Prostredná loď má čierny komín.
3. Anglická loď odchádza o deviatej.
4. Francúzska loď je vľavo od lodi vezúcej kávu a má modrý komín.
5. Vpravo od lodi vezúcej kakao je loď idúca do Marseille.
6. Brazílska loď ide do Manily.
7. Vedľa lodi vezúcej ryžu je loď so zeleným komínom.
8. Loď do Janova odchádza o piatej.
9. Španielska loď odchádza o siedmej a je vpravo od lodi idúcej do Marseille.
10. Do Hamburgu ide loď s červeným komínom.
11. Vedľa lodi odchádzajúcej o siedmej je loď s bielym komínom.
12. Loď kotviaca na kraji vezie obilie.
13. Loď s čiernym komínom odchádza o ôsmej.
14. Loď vezúca obilie kotví vedľa lodi vezúcej ryžu.
15. Do Hamburgu odchádza loď o šiestej.

member - zistenie prítomnosti prvku v zozname
member([x],List) - true-> x je prvkom zoznamu List

poradie = miesto1, miesto2, miesto3, miesto4, miesto5 
Lod - narodnost, cas, tovar, farba, pristav 

nexxto - je pravda ak LodB ide za LodA v zozname*/

vpravo(LodA,LodB,Zoznam):-nextto(LodA,LodB,Zoznam).
vedla(LodA,LodB,Zoznam):-nextto(LodA,LodB,Zoznam).
vedla(LodA,LodB,Zoznam):-nextto(LodB,LodA,Zoznam).

lodky(Port_Said,Caj):-		% pravidlo
	Lod = [_,_,[_,_,_,cierny,_],_,[_,_,obylie,_,_]], %ulozenie lodi - ich poradie, 2,12
    member([grecka,sest,kava,_,_],Lod), %zapis faktov, 1
    member([anglicka,devad,_,_,_],Lod), %3
    vpravo([francuzska,_,_,modry,_],[_,_,kava,_,_],Lod), %4
    vpravo([_,_,kakao,_,_],[_,_,_,_,marseille],Lod), %5
    member([brazilska,_,_,_,manila],Lod), %6
    vedla([_,_,ryza,_,_],[_,_,_,zeleny,_],Lod), %7
    member([_,pat,_,_,janov],Lod), %8
    vpravo([_,_,_,_,marseille],[spanielska,sedem,_,_,_],Lod), %9
    member([_,_,_,cerveny,hamburg],Lod), %10
    vedla([_,sedem,_,_,_],[_,_,_,biely,_],Lod), %11
    member([_,osem,_,cierny,_],Lod), %13
    vedla([_,_,obylie,_,_],[_,_,ryza,_,_],Lod), %14
    member([_,sest,_,_,hamburg],Lod), %15
	member([Port_Said,_,_,_,port_said],Lod), % akú národnosť má loď idúca do Port Saidu
    member([Caj,_,caj,_,_],Lod). %akú národnosť ma loď vezúca čaj
    